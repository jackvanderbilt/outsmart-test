<?php
	/**
	 * Leaf Class
	 */
	class Leaf {
		public $id;
		public $width;
		public $height;
		public $children;

		/**
		 * Leaf constructor
		 * 
		 * @param [type]  $id     [description]
		 * @param integer $width  [description]
		 * @param integer $height [description]
		 */
		public function __construct( $id, $width = 0, $height = 0, $children = [] )
		{
			$this->id = $id;
			$this->width = $width;
			$this->height = $height;
			$this->children = $children;
		}
	}

	$input = [
		new Leaf( 1, 100, 22, [
			new Leaf( 2, 865, 23 ),
			new Leaf( 3, 531, 98, [
				new Leaf( 4, 89, 958, [
					new Leaf( 5, 51, 81 ),
					new Leaf( 6, 91, 201, [
						new Leaf( 7, 54, 64 )
					]),
				]),
				new Leaf( 8, 5, 82 )
			]),
			new Leaf( 9, 12, 74 )
		]),
		new Leaf( 10, 546, 5 )
	];

	/**
	 * Recursive Function
	 */
	function calculateArea( array $leafs )
	{
		foreach( $leafs as $child )
		{
			$child->area = $child->width * $child->height;
			calculateArea( $child->children );
		}
	}

	/**
	 * Start recursive chain(s)
	 */
	calculateArea( $input );

	/**
	 * Print modified input (output)
	 */
	print_r( $input );
?>