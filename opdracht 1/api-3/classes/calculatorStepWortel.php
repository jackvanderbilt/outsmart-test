<?php
    require_once( __DIR__.'/calculatorStep.php' );

    final class CalculatorStepWortel extends CalculatorStep
    {
        public function execute( $baseValue )
        {
			if( $baseValue < 0 ) $this->error = 'previous step caused a number below zero';
				
            return $this->lastResult = sqrt( $baseValue );
        }
    }
?>