<?php
    require_once( __DIR__.'/calculatorStep.php' );

    final class CalculatorStepKwadraat extends CalculatorStep
    {
        public function execute( $baseValue )
        {
            if( $this->value == 1 ) $this->warning = "multiplying 1 by itself results in 1";
            
            return $this->lastResult = $baseValue * $baseValue;
        }
    }
?>