<?php
    require_once( __DIR__.'/calculatorStep.php' );

    final class CalculatorStepVermenigvuldigen extends CalculatorStep
    {
        public function execute( $baseValue )
        {
            if( $this->value == 1 ) $this->warning = "any number multiplied by 1 gives the same result";

            return $this->lastResult = $baseValue * $this->value;
        }
    }
?>