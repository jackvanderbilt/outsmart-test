<?php
    require_once( __DIR__.'/calculatorStep.php' );

    final class CalculatorStepAftrekken extends CalculatorStep
    {		
        public function execute( $baseValue )
        {
            if( $this->value < 0 ) $this->warning = 'subtracting a negative number adds';
            
            return $this->lastResult = $baseValue - $this->value;
        }
    }
?>