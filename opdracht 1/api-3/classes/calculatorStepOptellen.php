<?php
    require_once( __DIR__.'/calculatorStep.php' );

    final class CalculatorStepOptellen extends CalculatorStep
    {
        public function execute( $baseValue )
        {
            if( $this->value < 0 ) $this->warning = 'adding a negative number subtracts';
            
            return $this->lastResult = $baseValue + $this->value;
        }
    }
?>