<?php
    require( __DIR__.'/calculatorStepOptellen.php' );
    require( __DIR__.'/calculatorStepAftrekken.php' );
    require( __DIR__.'/calculatorStepVermenigvuldigen.php' );
    require( __DIR__.'/calculatorStepWortel.php' );
    require( __DIR__.'/calculatorStepKwadraat.php' );
    require( __DIR__.'/calculatorStepComplex.php' );
    
    class Calculator
    {
        public $baseValue;
        public $steps;
        public $lastResult;

        public function __construct( $baseValue = 0, $steps = [], $rawSteps = true, $calculate = false )
        {
            $this->baseValue = $baseValue;
            $this->steps = $rawSteps ? self::prepareRawSteps( $steps ) : $steps;
            $this->lastResult = $calculate ? $this->calculate() : NULL;
        }

        public static function prepareRawSteps( $steps )
        {
            return array_map( 'self::prepareRawStep', $steps );
        }

        public static function prepareRawStep( object $step )
        {
            switch ( $step->operation )
            {
                case 'optellen':
                    return new CalculatorStepOptellen( (float) $step->value ?? 0 );
                case 'aftrekken':
                    return new CalculatorStepAftrekken( (float) $step->value ?? 0 );
                case 'vermenigvuldigen':
                    return new CalculatorStepVermenigvuldigen( (float) $step->value ?? 1 );
                case 'wortel':
                    return new CalculatorStepWortel( (float) $step->value ?? null );
                case 'kwadraat':
                    return new CalculatorStepKwadraat( (float) $step->value ?? null );
                case 'complex':
                    return new CalculatorStepComplex( (float) $step->value ?? 0 );
            }
        }

        public function calculate()
        {
            $result = $this->baseValue;
            
            foreach( $this->steps as $step )
            {
                if ( !( $step instanceof CalculatorStep ) ) continue;
                
                if ( is_nan( $step->execute( $result ) ) )
                {
                    $result = NULL;
                    break;
                }
                else
                {
                    $result = $step->getLastResult();
                }
            }
            
            return $this->lastResult = $result;
        }
    }
?>