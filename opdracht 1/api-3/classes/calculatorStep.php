<?php
    abstract class CalculatorStep
    {
        public $value;
        protected $lastResult;
		public $error;
		public $warning;

        public function __construct( $value )
        {
            $this->value = $value;
        }

        public function execute( $baseValue )
        {
            $this->warning = "step does nothing with given base value";

            return $this->lastResult = $baseValue;
        }

        public function getLastResult()
        {
            return is_nan( $this->lastResult ) ? NULL : $this->lastResult;
        }
    }
?>