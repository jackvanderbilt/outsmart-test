<?php
    require_once( __DIR__.'/calculatorStep.php' );

    final class CalculatorStepComplex extends CalculatorStep
    {
        public function execute( $baseValue )
        {
            $result = $this->firstStep( $baseValue );
            $result = $this->secondStep( $result );

            $this->warning = "This step is an example step...";

            return $this->lastResult = $result;
        }

        private function firstStep( $value )
        {
            return $value * 1337;
        }        
        
        private function secondStep( $value )
        {
            return ( ceil( $value ) * pi() ) % 3;
        }

        /*
         * third step...
         * fourth step.....
         * fifth step...........
         * sixth step...............
         * 
         *  And probably some others functions too if this were to be an actually complex step
         */
    }
?>