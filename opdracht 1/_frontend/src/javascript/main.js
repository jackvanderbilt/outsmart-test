// Polyfills
import 'core-js/stable'
import 'regenerator-runtime/runtime'

// VueJS
import Vue from 'vue'
import App from '../App.vue'

document.addEventListener( 'DOMContentLoaded', () =>
{
    new Vue({
        render: ( h ) => h( App )
    }).$mount( document.querySelector( 'html > body > main#app' ) )
})
