<?php
    final class Calculator
    {
        public ?string $operation;
        public ?float $valueA;
        public ?float $valueB;
        public ?float $result;

        public function __construct( ?string $operation, ?float $valueA, ?float $valueB )
        {
            $this->operation = $operation;
            $this->valueA = $valueA;
            $this->valueB = $valueB;
            $this->result = $this->calculate();
        }
        
        public function calculate()
        {
            switch ( $this->operation )
            {
                case 'optellen':
                    return self::optellen();
                case 'aftrekken':
                    return self::aftrekken();
                case 'vermenigvuldigen':
                    return self::vermenigvuldigen();
                case 'wortel':
                    return self::wortel();
                case 'kwadraat':
                    return self::kwadraat();
                default: 
                    return null;
            }
        }

        public function optellen()
        {
            return $this->valueA + $this->valueB;
        }

        public function aftrekken()
        {
            return $this->valueA - $this->valueB;
        }

        public function vermenigvuldigen()
        {
            return $this->valueA * $this->valueB;
        }

        public function wortel()
        {
            if ( $this->valueA < 0 && $this->valueB < 0 )
			{
				return "Value should not be below zero";
            }

			return sqrt( $this->valueA > 0 ? $this->valueA : $this->valueB );
        }

        public function kwadraat()
        {
            return is_numeric( $this->valueA ) ? $this->valueA * $this->valueA : $this->valueB * $this->valueB;
        }
    }
?>