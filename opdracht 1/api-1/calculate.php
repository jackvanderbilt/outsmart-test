<?php
    require( __DIR__.'/classes/calculator.php' );
    
    header( 'Content-Type: application/json' );
	header( 'Access-Control-Allow-Origin: *' );

    $calculator = new Calculator( $_GET[ 'operation' ] ?? null, $_GET[ 'valueA' ] ?? null, $_GET[ 'valueB' ] ?? null );
	
    echo json_encode( [ 'result' => $calculator->result ] );
?>
