<?php
    require( __DIR__.'/classes/calculator.php' );
    require( __DIR__.'/classes/calculatorStep.php' );
	
    header( 'Content-Type: application/json' );
	header( 'Access-Control-Allow-Origin: *' );

    // Check if all URL parameters have been provided
    if ( !isset( $_GET[ 'baseValue' ], $_GET[ 'steps' ] ) )
    {
        http_response_code( 400 );
        
        echo json_encode( [
            'error' => "Expected 'baseValue' and 'steps' parameters to be set", 
            'debug' => [ 
                'baseValue' => $_GET[ 'baseValue' ] ?? NULL, 
                'steps' => $_GET[ 'steps' ] ?? NULL
            ]
        ] );

        return;
    }

    // Local variables from url parameters
    $baseValue = $_GET[ 'baseValue' ];
    $steps = json_decode( $_GET[ 'steps' ] );

    // Check if given parameters are as expected
    if( !is_numeric( $baseValue ) || !is_array( $steps ) )
    {
        http_response_code( 400 );
        
        echo json_encode( [
            'error' => "Expected 'baseValue' to be numeric and 'steps' to be a valid json parsable array", 
            'debug' => [ 
                'base-value' => is_numeric( $baseValue ), 
                'steps' => is_array( $steps )
            ]
        ] );

        return;
    }

    // Check if given steps are valid
    foreach( $steps as $step )
    {
        if( !isset( $step->operation ) || empty( $step->operation ) || ( isset( $step->value ) && !is_numeric( $step->value ) ) )
        {
            http_response_code( 400 );
            
            echo json_encode( [
                'error' => "Invalid step provided in steps array", 
                'debug' => [ 
                    'step' => $step
                ]
            ] );

            return;
        }
    }
    
    // Create calculator and calculate using provided steps
    $calculator = new Calculator( $baseValue,  array_map( fn( $step ) => new CalculatorStep( $step->operation, $step->value ?? NULL ), $steps ) );
    $calculator->calculate();

    // Check if calculation is invalid
    if ( $calculator->lastResult === NULL )
    {
        http_response_code( 400 );

        echo json_encode( [ 
            'error' => 'Calculation failed due to invalid operation on previous step result and/or invalid values.',
            'debug' => [ 
                'base-value' => $baseValue,
                'steps' => $steps,
                'calculator' => $calculator
            ]
        ] );

        return;
    }

    // return result
    echo json_encode( [ 'result' => $calculator->lastResult ] );
?>
