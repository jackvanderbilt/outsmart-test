<?php
    class CalculatorStep
    {
        public $operation;
        public $value;

        public function __construct( $operation, $value )
        {
            $this->operation = $operation;
            $this->value = $value;
        }
    }
?>