<?php
    class Calculator
    {
        public $baseValue;
        public $steps;
        public $lastResult;

        public function __construct( $baseValue, $steps )
        {
            $this->baseValue = $baseValue;
            $this->steps = $steps;
            $this->lastResult = NULL;
        }
        
        public function calculate()
        {
            $result = $this->baseValue;

            foreach( $this->steps as $step )
            {
                switch ( $step->operation )
                {
                    case 'optellen':
                        $result += $step->value;
                        break 1;
                    case 'aftrekken':
                        $result -= $step->value;
                        break 1;
                    case 'vermenigvuldigen':
                        $result *= $step->value;
                        break 1;
                    case 'wortel':
                        $result = sqrt( $result );
                        break 1;
                    case 'kwadraat':
                        $result *= $result;
                        break 1;
                    case 'complex':
                        $result = ceil( $result * 1337 ) * pi() % 3;
                        break 1;
                    default:
                        $result = NULL;
                        break 2;
                }
                
                if ( is_nan( $result ) )
                {
                    $result = NULL;
                    break 1;
                }
            }

            return $this->lastResult = $result;
        }
    }
?>